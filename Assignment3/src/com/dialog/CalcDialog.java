package com.dialog;

import com.example.assignment3.R;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
/*
 * Calculator
 */
public class CalcDialog extends Dialog implements android.view.View.OnClickListener{

	Button one, two, three, four, five, six, seven, eight, nine, zero, add, sub, mul, div, cancel, equal;
	EditText disp;
	int op1, op2;
	String optr;

	public CalcDialog(Context context) {
		super(context);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.calc);
		disp = (EditText)findViewById(R.id.display);
		LinearLayout parent = (LinearLayout)findViewById(R.id.linearLayoutCalc);
		// Set Onclick Listeners with for loop
		for (int i = 0; i < parent.getChildCount(); i++){
			View child = parent.getChildAt(i);
			if (!(child instanceof LinearLayout)) continue;
			LinearLayout layout = (LinearLayout)child;
			for (int j = 0; j < layout.getChildCount(); j++){
				View gChild = layout.getChildAt(j);
				if (!(gChild instanceof Button)) continue;
				Button button = (Button)gChild;
				button.setOnClickListener(this);
			}
		}
		Button cancel = (Button)findViewById(R.id.buttonClose);
		cancel.setOnClickListener(this);
	}
	public void operation(){
	   if(optr.equals("+")){
	       op2 = Integer.parseInt(disp.getText().toString());
	       disp.setText("");
	       op1 = op1 + op2;
	       disp.setText(Integer.toString(op1));
	   }
	   else if(optr.equals("-")){
	       op2 = Integer.parseInt(disp.getText().toString());
	       disp.setText("");
	       op1 = op1 - op2;
	       disp.setText(Integer.toString(op1));
	   }
	   else if(optr.equals("*")){
	       op2 = Integer.parseInt(disp.getText().toString());
	       disp.setText("");
	       op1 = op1 * op2;
	       disp.setText(Integer.toString(op1));
	   }
	   else if(optr.equals("/")){
	       op2 = Integer.parseInt(disp.getText().toString());
	       disp.setText("");
	       op1 = op1 / op2;
	       disp.setText(Integer.toString(op1));
       }
	}
	@Override
	public void onClick(View src) {
		Editable str =  disp.getText();
		Button btn = (Button)src;
		if (btn == null) return;
	    switch(btn.getId()){
	    	case R.id.zero:
	    	case R.id.one:
	    	case R.id.two:
	    	case R.id.three:
	    	case R.id.four:
	    	case R.id.five:
	    	case R.id.six:
	    	case R.id.seven:
	    	case R.id.eight:
	    	case R.id.nine:
		        if(op2 != 0){
		            op2 = 0;
		            disp.setText("");
		        }
		        str = str.append(btn.getText());
		        disp.setText(str);
	        	break;
		    case R.id.cancel:
		        op1 = 0;
		        op2 = 0;
		        disp.setText("");
		        break;
		    case R.id.add:
		        optr = "+";
		        if(op1 == 0){
		            op1 = Integer.parseInt(disp.getText().toString());
		            disp.setText("");
		        }
		        else if(op2 != 0){
		            op2 = 0;
		            disp.setText("");
		        }
		        else{
		            op2 = Integer.parseInt(disp.getText().toString());
		            disp.setText("");
		            op1 = op1 + op2;
		            disp.setText(Integer.toString(op1));
		        }
		        break;
		    case R.id.sub:
		        optr = "-";
		        if(op1 == 0){
		            op1 = Integer.parseInt(disp.getText().toString());
		            disp.setText("");
		        }
		        else if(op2 != 0){
		            op2 = 0;
		            disp.setText("");
		        }
		        else{
		            op2 = Integer.parseInt(disp.getText().toString());
		            disp.setText("");
		            op1 = op1 - op2;
		            disp.setText(Integer.toString(op1));
		        }
		        break;
		    case R.id.mul:
		        optr = "*";
		        if(op1 == 0){
		            op1 = Integer.parseInt(disp.getText().toString());
		            disp.setText("");
		        }
		        else if(op2 != 0){
		            op2 = 0;
		            disp.setText("");
		        }
		        else{
		            op2 = Integer.parseInt(disp.getText().toString());
		            disp.setText("");
		            op1 = op1 * op2;
		            disp.setText(Integer.toString(op1));
		        }
		        break;
		    case R.id.div:
		        optr = "/";
		        if(op1 == 0){
		            op1 = Integer.parseInt(disp.getText().toString());
		            disp.setText("");
		        }
		        else if(op2 != 0){
		            op2 = 0;
		            disp.setText("");
		        }
		        else{
		            op2 = Integer.parseInt(disp.getText().toString());
		            disp.setText("");
		            op1 = op1 / op2;
		            disp.setText(Integer.toString(op1));
		        }
		        break;
		    case R.id.equal:
		        if(!optr.equals(null)){
		            if(op2 != 0){
		                if(optr.equals("+")){
		                    disp.setText("");
		                    /*op1 = op1 + op2;*/
		                    disp.setText(Integer.toString(op1));
		                }
		                else if(optr.equals("-")){
		                    disp.setText("");/*
		                    op1 = op1 - op2;*/
		                    disp.setText(Integer.toString(op1));
		                }
		                else if(optr.equals("*")){
		                    disp.setText("");/*
		                    op1 = op1 * op2;*/
		                    disp.setText(Integer.toString(op1));
		                }
		                else if(optr.equals("/")){
		                    disp.setText("");/*
		                    op1 = op1 / op2;*/
		                    disp.setText(Integer.toString(op1));
		                }
		            }
		            else{
		                operation();
		            }
		        }
		        break;
		    case R.id.buttonClose:
		    	this.dismiss();
	        default: break;
	    }
	}
}  
		
