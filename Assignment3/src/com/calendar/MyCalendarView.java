package com.calendar;

import android.content.Context;
import android.widget.CalendarView;
/*
 * Original class which inherits CalendarView to extend 
 */
public class MyCalendarView extends CalendarView {

	public MyCalendarView(Context context) {
		super(context);
		setShowWeekNumber(false);
	}
}
