package com.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

/*
 * Original class which inherites SQLiteOpenHelper
 */
public class DBHelper extends SQLiteOpenHelper {
	
	private final static String DB_NAME = "assingment3.db";
	private final static int DB_VERSION = 1;
	
	protected SQLiteDatabase db;
	// Singleton Pattern
	private static DBHelper instance = null;
	public static DBHelper getInstance(Context context){
		if (instance == null){
			instance = new DBHelper(context);
		}
		return instance;
	}
	// Alternative constructors
	private DBHelper(Context context){
		this(context, DB_NAME, null, DB_VERSION);
	}
	public DBHelper(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
	}
}
