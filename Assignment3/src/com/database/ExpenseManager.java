package com.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
/*
 * Database manager to manage Expense table
 */
public class ExpenseManager{
	
	private Context context;
	public final String TABLE_NAME = "Expense";
	public final String COL_ID = "expense_id";
	public final String COL_NAME = "expense_name";
	public final String COL_DATE = "expense_date";
	public final String COL_AMOUNT = "expense_amount";
	public final String COL_PLAN_ID = "plan_id";
	
	public DBHelper helper;
	public SQLiteDatabase db;

	
	private static ExpenseManager instance;
	private ExpenseManager(Context context){
		this.context = context;
		helper = DBHelper.getInstance(context);
		db = helper.getWritableDatabase();
		createTable();
	}
	public static ExpenseManager getInstance(Context context){
		if (instance == null){
			instance = new ExpenseManager(context);
		}
		return instance;
	}	
	
	public void createTable(){
	      db.execSQL(
	        "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
	        + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
	        + COL_AMOUNT + " REAL DEFAULT 0,"
	        + COL_NAME + " TEXT NOT NULL,"
	        + COL_DATE + " TEXT NOT NULL,"
	        + COL_PLAN_ID + " INTEGER DEFAULT 0,"
	        + "FOREIGN KEY(" + COL_PLAN_ID + ") REFERENCES Plan(plan_id)"
	        + ");");
	}

	public int update(ContentValues values, String whereClause, String[] whereArgs) {
		return db.update(TABLE_NAME, values, whereClause, whereArgs);
	}

	public void delete(int id) {
		db.rawQuery("delete from " + TABLE_NAME + " where " + COL_ID + " = ?;", new String[]{Integer.toString(id)}).moveToFirst();
	}

	public int insert(ContentValues values) {
		int result = 0;
		try{
			result = (int)db.insertOrThrow(TABLE_NAME, null, values);
		}catch (Exception ex){
			System.out.println(ex.getMessage());
		}
		return result;
	}
	public Cursor select(int id){
		return db.rawQuery("select * from " + TABLE_NAME + " where " + COL_ID + " = ?;", new String[]{Integer.toString(id)});
	}
	public Cursor selectAll(String date) {
		return db.rawQuery("select * from " + TABLE_NAME + " where " + COL_DATE + " = ?;", new String[]{date});
	}
	public Cursor selectAll(int month, int year) {
		return this.selectAll(Integer.toString(month), Integer.toString(year));
	}
	public Cursor selectAll(String month, String year) {
		String date = month + "/" + year;
		Cursor c = db.rawQuery("select substr(" + COL_DATE +  ",1,3) " + " from " + TABLE_NAME + ";", null);
		if (c.moveToFirst()){
			String s = c.getString(0);
			if (s == "") { s = "aaa";}
		}
		return db.rawQuery("select * from " + TABLE_NAME 
						 + " where substr(" + COL_DATE + ",4,7) " 
						 + " = " + "?;",
					 	 new String[]{date});
	}
}
