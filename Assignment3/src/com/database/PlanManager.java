package com.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
/*
 * Database manager to manage Plan table
 */
public class PlanManager{
	
	private Context context;
	public final String TABLE_NAME = "Plan";
	public final String COL_ID = "plan_id";
	public final String COL_NAME = "plan_name";
	public final String COL_DATE = "plan_date";
	public final String COL_TIME = "plan_time";
	public final String COL_DESCRIPTION = "plan_description";
	public final String COL_ALARM = "alarm_set";
	public DBHelper helper;
	public SQLiteDatabase db;

	
	private static PlanManager instance;
	private PlanManager(Context context){
		this.context = context;
		helper = DBHelper.getInstance(context);
		db = helper.getWritableDatabase();
		createTable();
	}
	public static PlanManager getInstance(Context context){
		if (instance == null){
			instance = new PlanManager(context);
		}
		return instance;
	}	
	
	public void createTable(){
	      db.execSQL(
	        "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
	        + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
	        + COL_NAME + " TEXT NOT NULL,"
	        + COL_DATE + " TEXT NOT NULL,"
	        + COL_TIME + " TEXT NOT NULL,"
	        + COL_DESCRIPTION + " TEXT,"
	        + COL_ALARM + " INTEGER);");
	}

	public int update(ContentValues values, String whereClause, String[] whereArgs) {
		return db.update(TABLE_NAME, values, whereClause, whereArgs);
	}

	public void delete(int id) {
		db.rawQuery("delete from " + TABLE_NAME + " where " + COL_ID + " = ?;", new String[]{Integer.toString(id)}).moveToFirst();
	}

	public int insert(ContentValues values) {
		int result = 0;
		try{
			result = (int)db.insertOrThrow(TABLE_NAME, null, values);
		}catch (Exception ex){
			System.out.println(ex.getMessage());
		}
		return result;
	}
	public Cursor select(int id){
		return db.rawQuery("select * from " + TABLE_NAME + " where " + COL_ID + " = ?;", new String[]{Integer.toString(id)});
	}
	public Cursor selectAll(String date) {
		return db.rawQuery("select * from " + TABLE_NAME + " where " + COL_DATE + " = ?;", new String[]{date});
		
	}
}
