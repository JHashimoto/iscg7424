package com.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
/*
 * Database manager to manage budget table
 */
public class BudgetManager{
	
	private Context context;
	public final String TABLE_NAME = "Budget";
	public final String COL_ID = "budget_id";
	public final String COL_AMOUNT = "budget_amount";
	public final String COL_YEAR = "budget_year";
	public final String COL_MONTH = "budget_month";
	
	public DBHelper helper;
	public SQLiteDatabase db;
	
	// Singleton pattern
	private static BudgetManager instance;
	private BudgetManager(Context context){
		this.context = context;
		helper = DBHelper.getInstance(context);
		db = helper.getWritableDatabase();
		createTable();
	}
	public static BudgetManager getInstance(Context context){
		if (instance == null){
			instance = new BudgetManager(context);
		}
		return instance;
	}	
	
	public void createTable(){
	      db.execSQL(
	        "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ("
	        + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
	        + COL_AMOUNT + " REAL,"
	        + COL_YEAR + " INTEGER,"
	        + COL_MONTH + " INTEGER);");
	}

	public void update(ContentValues values, int year, int month, String amount) {
		String whereClause = COL_YEAR + " = ?" + " AND " + COL_MONTH + " = ?;";
		String[] whereArgs = new String[]{Integer.toString(year),Integer.toString(month)}; 
		db.update(TABLE_NAME, values, whereClause, whereArgs);		
	}

	public void delete(int id) {
		db.rawQuery("delete from " + TABLE_NAME + " where " + COL_ID + " = ?;", new String[]{Integer.toString(id)}).moveToFirst();
	}

	public int insert(ContentValues values) {
		int result = 0;
		try{
			result = (int)db.insertOrThrow(TABLE_NAME, null, values);
		}catch (Exception ex){
			System.out.println(ex.getMessage());
		}
		return result;
	}
	public Cursor select(int year, int month){
		return db.rawQuery("select * from " + TABLE_NAME 
						 + " where "
						 + COL_YEAR + "  = ?" + " AND " + COL_MONTH + " = ?;",
						 new String[]{Integer.toString(year), Integer.toString(month)});
	}
}
