package com.example.assignment3;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import com.database.ExpenseManager;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
/*
 * Listing expenses of selected day
 */
public class ExpenseActivity extends Activity implements OnClickListener, OnItemClickListener{
	private Date date;
	private int selectedIndex = -1;
	private ExpenseManager manager;

	private ListView listView;
	private List<String> expenseList = new ArrayList<String>();
	private ArrayAdapter<String> expenseAdapter;
	
	private HashMap<Integer, Integer> indexIdMap = new HashMap<Integer, Integer>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_expense);
		// Set selected date
		Intent i = getIntent();
		date = new Date(i.getLongExtra("Date", 0));
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", new Locale("en", "NZ"));
		((TextView)findViewById(R.id.textViewDateExpense)).setText(sdf.format(date));
		
		manager = MainActivity.getExpenseManager();
		
		listView = (ListView)findViewById(R.id.listViewExpense);
		setAdapter();
		setExpenses();
		listView.setOnItemClickListener(this);
		listView.setItemsCanFocus(true);
		listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

		Button add = (Button)findViewById(R.id.buttonAddExpense);
		add.setOnClickListener(this);
	}
	private void setAdapter(){
		expenseAdapter = new ArrayAdapter<String>(
				this, android.R.layout.simple_list_item_1, expenseList);
		listView.setAdapter(expenseAdapter);
	}
	/*
	 * Set expenses to the listview
	 */
	private void setExpenses(){
		expenseAdapter.clear();
		indexIdMap.clear();
		Cursor cursor = manager.selectAll(((TextView)findViewById(R.id.textViewDateExpense)).getText().toString());
		int index = 0;
		if (cursor.moveToFirst()){
			do{
				StringBuilder builder = new StringBuilder();
				builder.append(cursor.getString(cursor.getColumnIndex(manager.COL_NAME)));
				builder.append("\n");
				builder.append("$");
				builder.append(cursor.getDouble(cursor.getColumnIndex(manager.COL_AMOUNT)));
				expenseAdapter.add(builder.toString());
				
				indexIdMap.put(index, cursor.getInt(cursor.getColumnIndex(manager.COL_ID)));		
				index++;
			} while(cursor.moveToNext());
		}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.expense_menu, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id;
        switch (item.getItemId()) {
        case R.id.item_editExpense:
        	if (selectedIndex < 0) break;
        	id = indexIdMap.get(selectedIndex);
        	showExpenseEditActivity(id);
            break;
        case R.id.item_deleteExpense:
        	if (selectedIndex < 0) break;
        	id = indexIdMap.get(selectedIndex);
        	deleteExpense(id);
        	listView.invalidate();
            break;
        default:
        	break;
        }
		return super.onOptionsItemSelected(item);
	}
	private void showExpenseEditActivity(){
		showExpenseEditActivity(-1);
	}
	private void showExpenseEditActivity(int id){
		Intent intent = new Intent(getApplicationContext(), ExpenseEditActivity.class);
		intent.putExtra("date", ((TextView)findViewById(R.id.textViewDateExpense)).getText().toString());
		intent.putExtra("id", id);
		startActivityForResult(intent, 0);
	}
	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
		selectedIndex = position;
		openOptionsMenu();
		listView.setItemChecked(position, false);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId())
		{
			case R.id.buttonAddExpense:
				showExpenseEditActivity();
		}		
	}
	private void deleteExpense(int id){
		manager.delete(id);
		setExpenses();
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode > 0){
			setExpenses();
		}
		setResult(resultCode);
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if(keyCode==KeyEvent.KEYCODE_BACK){
	    	finish();
	      }
	      return true;
	}	
}
