package com.example.assignment3;

import java.util.Calendar;
import java.util.Locale;

import android.os.Bundle;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.CalendarView.OnDateChangeListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.calendar.*;
import com.database.BudgetManager;
import com.database.ExpenseManager;
import com.database.PlanManager;
import com.example.assignment3.R;

public class MainActivity extends Activity implements OnClickListener, OnDateChangeListener, OnEditorActionListener{

	private BudgetManager budgetManager;
	public static PlanManager planManager;
	public static ExpenseManager expenseManager;
	private MyCalendarView calendar;
	
	private EditText budget;
	private TextView balance;
	
	private int currentYear;
	private int currentMonth;
	
	public static PlanManager getPlanManager(){
		return planManager;
	}
	public static ExpenseManager getExpenseManager() {
		return expenseManager;
	}	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		budgetManager = BudgetManager.getInstance(this);
		planManager = PlanManager.getInstance(this);
		expenseManager = ExpenseManager.getInstance(this);
		
		budget = (EditText)findViewById(R.id.EditTextBudgetOfMonth);
		budget.setOnEditorActionListener(this);
		// set calendar view
		setCalendar();
		balance = (TextView)findViewById(R.id.textViewBalance);
		currentMonth = Calendar.getInstance(new Locale("en", "NZ")).get(Calendar.MONTH)+1;
		currentYear = Calendar.getInstance(new Locale("en", "NZ")).get(Calendar.YEAR);
		setBudgetOfCurrentMonth(currentYear, currentMonth);
		setBalanceOfCurrentMonth(currentYear, currentMonth);
		
		Button btn = (Button)findViewById(R.id.buttonShowPlans);
		btn.setOnClickListener(this);
		btn = (Button)findViewById(R.id.buttonShowExpenses);
		btn.setOnClickListener(this);
	}
	private void setCalendar(){
		FrameLayout layout = (FrameLayout)findViewById(R.id.frameLayoutCalendar);
		calendar = new MyCalendarView(this);
		layout.addView(calendar, LayoutParams.MATCH_PARENT);
		calendar.setOnDateChangeListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.item_showPlans:
        	showPlanActivity();
            break;
        case R.id.item_showExpenses:
        	showExpenseActivity();
            break;
        case R.id.item_exit:
        	this.finish();
            break;
        default:
        	break;
        }
		return super.onOptionsItemSelected(item);
	}
	private void showPlanActivity(){
		long date = calendar.getDate();
		Intent intent = new Intent(getApplicationContext(), PlanActivity.class);
		intent.putExtra("Date", date);
		//startActivity(intent);
		startActivityForResult(intent, 0);
	}
	private void showExpenseActivity(){
		long date = calendar.getDate();
		Intent intent = new Intent(getApplicationContext(), ExpenseActivity.class);
		intent.putExtra("Date", date);
		//startActivity(intent);
		startActivityForResult(intent, 1);
	}
	@Override
	public void onSelectedDayChange(CalendarView view, int year, int month,
			int dayOfMonth) {
		currentYear = year;
		currentMonth = month+1;
		setBudgetOfCurrentMonth(year, month+1);
		setBalanceOfCurrentMonth(year, month+1);
	}
	private void setBudgetOfCurrentMonth(int year, int month){
		Cursor cursor = budgetManager.select(year, month);
		if (cursor.moveToFirst()){
			double amount = cursor.getDouble(cursor.getColumnIndex(budgetManager.COL_AMOUNT));
			budget.setText(Double.toString(amount));
		}else{
		    ContentValues values = new ContentValues();
		    values.put(budgetManager.COL_YEAR, Integer.toString(year));
		    values.put(budgetManager.COL_MONTH, Integer.toString(month));
		    values.put(budgetManager.COL_AMOUNT, "0.0");
		    budgetManager.insert(values);
			budget.setText("0.0");
		}
	}
	private void setBalanceOfCurrentMonth(int year, int month){
		Cursor cursor = expenseManager.selectAll(month, year);
		double totalBdgt = Double.parseDouble(budget.getText().toString());
		double total = 0;
		if (cursor.moveToFirst()){
			do{
				total += cursor.getDouble(cursor.getColumnIndex(expenseManager.COL_AMOUNT));
			} while(cursor.moveToNext());
		}
		double balanceAmnt = totalBdgt - total;
		balance.setText(" " + Double.toString(balanceAmnt));
		if (balanceAmnt < 0){
			balance.setTextColor(Color.RED);
		}else{
			balance.setTextColor(Color.WHITE);
		}		
	}
	@Override
	public boolean onEditorAction(TextView view, int arg1, KeyEvent arg2) {
		// Update/Insert to DB
	    ContentValues values = new ContentValues();
	    values.put(budgetManager.COL_AMOUNT, ((EditText)view).getText().toString());
	    budgetManager.update(values, currentYear, currentMonth, ((EditText)view).getText().toString());
	    
	    setBudgetOfCurrentMonth(currentYear, currentMonth);
	    setBalanceOfCurrentMonth(currentYear, currentMonth);
	    
		return false;
	}
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.buttonShowPlans:
			showPlanActivity();
			break;
		case R.id.buttonShowExpenses:
			showExpenseActivity();
			break;
		default:
			break;
		}
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode > 0){
		    setBalanceOfCurrentMonth(currentYear, currentMonth);
		}
	}
}
