package com.example.assignment3;

import com.database.ExpenseManager;
import com.dialog.CalcDialog;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
/*
 * Editing the contents of the selected expense
 */
public class ExpenseEditActivity extends Activity implements OnClickListener {
	private String date;
	private int expenseId;
	private EditText name;
	private EditText amount;
	
	private ExpenseManager manager;
	private CalcDialog dialog;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.expense);
		
		manager = MainActivity.getExpenseManager();

		date = getIntent().getStringExtra("date");
		expenseId = getIntent().getIntExtra("id", -1);
		name = (EditText)findViewById(R.id.editTextExpenseName);
		amount = (EditText)findViewById(R.id.editTextExpenseAmount);
		
		dialog = new CalcDialog(this);
		ImageButton calcBtn = (ImageButton)findViewById(R.id.imageButtonCalc);
		calcBtn.setOnClickListener(this);
		Button btn = (Button)findViewById(R.id.buttonExpenseDone);
		btn.setOnClickListener(this);
		btn = (Button)findViewById(R.id.buttonExpenseCancel);
		btn.setOnClickListener(this);
		
		setContent();
	}
	private void setContent(){
		if (expenseId < 0) return;
		Cursor cursor = manager.select(expenseId);
        if(cursor.moveToFirst()){
        	name.setText(cursor.getString(cursor.getColumnIndex(manager.COL_NAME)));
        	amount.setText(cursor.getString(cursor.getColumnIndex(manager.COL_AMOUNT)));
        }
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
		case R.id.imageButtonCalc:
            dialog.show();
            break;
		case R.id.buttonExpenseDone:
			if (expenseId < 0){
				addExpense();
			}else{
				updateExpense();
			}
			setResult(1);
			finish();
			break;
		case R.id.buttonExpenseCancel:
			finish();
			break;
		}
	}
	private void addExpense(){
	    ContentValues values = new ContentValues();
	    values.put(manager.COL_NAME, name.getText().toString());
	    values.put(manager.COL_DATE, date);
	    values.put(manager.COL_AMOUNT, Double.parseDouble(amount.getText().toString()));
	    //values.put(manager.COL_PLAN_ID, 0);	// to the future improvement
	    
	    manager.insert(values);
	}
	private void updateExpense(){
	    ContentValues values = new ContentValues();
	    values.put(manager.COL_NAME, name.getText().toString());
	    values.put(manager.COL_DATE, date);
	    values.put(manager.COL_AMOUNT, Double.parseDouble(amount.getText().toString()));
	    //values.put(manager.COL_PLAN_ID, 0); // to the future improvement

	    manager.update(values, manager.COL_ID + " = ?", new String[]{Integer.toString(expenseId)});
	}
}
