package com.example.assignment3;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.database.PlanManager;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.ToggleButton;
/*
 * Editing the contents of the selected expense
 */
public class PlanEditActivity extends Activity implements OnClickListener{
	private String date;
	private int planId = -1;
	private PlanManager manager;
	private EditText name;
	private TextView timeSet;
	private EditText description;
	private ToggleButton isAlarmSet;
	
	private boolean originalAlarm = false;
	
    private TimePickerDialog setTimeDialog;
    TimePickerDialog.OnTimeSetListener onTimeSetListener = new OnTimeSetListener() {
		@Override
		public void onTimeSet(TimePicker picker, int h, int m) {
			String hour = String.format("%02d", h);
			String minute = String.format("%02d", m);
			timeSet.setText(hour + ":" + minute);
		}
    };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.plan);
		
		manager = MainActivity.getPlanManager();
		
		date = getIntent().getStringExtra("date");
		planId = getIntent().getIntExtra("id", -1);
		name = (EditText)findViewById(R.id.editTextPlanName);
		timeSet = (TextView)findViewById(R.id.textViewTimeSet);
		timeSet.setTextColor(Color.BLACK);
		description = (EditText)findViewById(R.id.editTextDescription);
		isAlarmSet = (ToggleButton)findViewById(R.id.toggleButtonAlarm);
		
		Button btn = (Button)findViewById(R.id.buttonTimeSet);
		btn.setOnClickListener(this);
		btn = (Button)findViewById(R.id.buttonPlanDone);
		btn.setOnClickListener(this);
		btn = (Button)findViewById(R.id.buttonPlanCancel);
		btn.setOnClickListener(this);
		
		setContent();
	}
	private void setContent(){
		if (planId < 0) return;
		Cursor cursor = manager.select(planId);

        if(cursor.moveToFirst()){
        	name.setText(cursor.getString(cursor.getColumnIndex(manager.COL_NAME)));
        	timeSet.setText(cursor.getString(cursor.getColumnIndex(manager.COL_TIME)));
        	description.setText(cursor.getString(cursor.getColumnIndex(manager.COL_DESCRIPTION)));
        	originalAlarm = (cursor.getInt(cursor.getColumnIndex(manager.COL_ALARM )) == 0)? false : true;
        	isAlarmSet.setChecked(originalAlarm);
        }
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
		case R.id.buttonTimeSet:
			showTimePickerDialog();
			break;
		case R.id.buttonPlanDone:
			if (planId < 0){
				try {
					addPlan();
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}else{
				try {
					updatePlan();
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			setResult(1);
			finish();
			break;
		case R.id.buttonPlanCancel:
			finish();
			break;
		}
	}
	private void showTimePickerDialog(){
        Calendar calendar = Calendar.getInstance();
        int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        setTimeDialog = new TimePickerDialog(this, android.R.style.Theme_Black,
                			 				 onTimeSetListener, hourOfDay, minute, true);
        setTimeDialog.show();
	}
	private void addPlan() throws ParseException{
	    ContentValues values = new ContentValues();
	    values.put(manager.COL_NAME, name.getText().toString());
	    values.put(manager.COL_DATE, date);
	    values.put(manager.COL_TIME, timeSet.getText().toString());
	    values.put(manager.COL_DESCRIPTION, description.getText().toString());
	    int alarm = (isAlarmSet.isChecked())? 1 : 0;
	    values.put(manager.COL_ALARM, alarm);
	    if (alarm > 0){
	    	setAlarm();
	    }
	    
	    manager.insert(values);
	}
	private void updatePlan() throws ParseException{
	    ContentValues values = new ContentValues();
	    values.put(manager.COL_NAME, name.getText().toString());
	    values.put(manager.COL_DATE, date);
	    values.put(manager.COL_TIME, timeSet.getText().toString());
	    values.put(manager.COL_DESCRIPTION, description.getText().toString());
	    int alarm = (isAlarmSet.isChecked())? 1 : 0;
	    values.put(manager.COL_ALARM, alarm);
	    if (originalAlarm && (alarm == 0)){
	    	cancelAlarm();
	    }else if (!originalAlarm && (alarm == 1)){
	    	setAlarm();
	    }	    
	    manager.update(values, manager.COL_ID + " = ?", new String[]{Integer.toString(planId)});
	}
	private void setAlarm() throws ParseException{
		AlarmManager am = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(this, PlanEditActivity.class);
		PendingIntent pi = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyyHH:mm", new Locale("en", "NZ"));
		Date alDate = sdf.parse(date + timeSet.getText().toString());
		
		am.set(am.RTC_WAKEUP, alDate.getTime(), pi);
	}
	private void cancelAlarm(){
		AlarmManager am = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(this, PlanEditActivity.class);
		PendingIntent pi = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, 0);
		am.cancel(pi);
	}
}
