package com.example.assignment3;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.BaseInputConnection;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.database.*;
/*
 * Listing plans of selected day
 */
public class PlanActivity extends Activity implements OnClickListener, OnItemClickListener{

	private Date date;
	private int selectedIndex = -1;
	
	private ListView listView;
	private List<String> planList = new ArrayList<String>();
	private ArrayAdapter<String> planAdapter;
	
	private PlanManager manager;
	private HashMap<Integer, Integer> indexIdMap = new HashMap<Integer, Integer>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_plan);
		// Set selected date
		Intent i = getIntent();
		date = new Date(i.getLongExtra("Date", 0));
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", new Locale("en", "NZ"));
		((TextView)findViewById(R.id.textViewDatePlan)).setText(sdf.format(date));
		
		manager = MainActivity.getPlanManager();
		
		listView = (ListView)findViewById(R.id.listViewPlan);
		setAdapter();
		setPlans();
		listView.setOnItemClickListener(this);
		listView.setItemsCanFocus(true);
		listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		
		Button add = (Button)findViewById(R.id.buttonAddPlan);
		add.setOnClickListener(this);
	}
	private void setAdapter(){
		planAdapter = new ArrayAdapter<String>(
				this, android.R.layout.simple_list_item_1, planList);
		listView.setAdapter(planAdapter);
	}
	private void setPlans(){
		planAdapter.clear();
		indexIdMap.clear();
		Cursor cursor = manager.selectAll(((TextView)findViewById(R.id.textViewDatePlan)).getText().toString());
		int index = 0;
		if (cursor.moveToFirst()){
			do{
				StringBuilder builder = new StringBuilder();
				builder.append(cursor.getString(cursor.getColumnIndex(manager.COL_TIME)));
				builder.append("\n");
				builder.append(cursor.getString(cursor.getColumnIndex(manager.COL_NAME)));
				builder.append("\n");
				builder.append("ALARM: ");
				builder.append((cursor.getInt(cursor.getColumnIndex(manager.COL_ALARM)) == 1)? "ON" : "OFF");
				planAdapter.add(builder.toString());
								
				indexIdMap.put(index, cursor.getInt(cursor.getColumnIndex(manager.COL_ID)));		
				index++;
			} while(cursor.moveToNext());
		}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.plan_menu, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id;
        switch (item.getItemId()) {
        case R.id.item_editPlan:
        	if (selectedIndex < 0) break;
        	id = indexIdMap.get(selectedIndex);
        	showPlanEditActivity(id);
            break;
        case R.id.item_deletePlan:
        	if (selectedIndex < 0) break;
        	id = indexIdMap.get(selectedIndex);
        	deletePlan(id);
        	listView.invalidate();
            break;
        default:
        	break;
        }
		return super.onOptionsItemSelected(item);
	}
	private void showPlanEditActivity(){
		showPlanEditActivity(-1);
	}
	private void showPlanEditActivity(int id){
		Intent intent = new Intent(getApplicationContext(), PlanEditActivity.class);
		intent.putExtra("date", ((TextView)findViewById(R.id.textViewDatePlan)).getText().toString());
		intent.putExtra("id", id);
		startActivityForResult(intent, 0);
	}
	@Override
	public void onClick(View v) {
		switch (v.getId())
		{
			case R.id.buttonAddPlan:
				showPlanEditActivity();
		}
	}
	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
		selectedIndex = position;
		openOptionsMenu();
		listView.setItemChecked(position, false);
	}
	private void deletePlan(int id){
		manager.delete(id);
		setPlans();
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode > 0){
			setPlans();
		}
		setResult(resultCode);
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// 
	    if(keyCode==KeyEvent.KEYCODE_BACK){
	    	finish();
	      }
	      return true;
	}	
}
