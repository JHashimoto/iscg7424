package com.example.testlayout;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.MotionEvent;
import android.view.View;

//描画を行うView
public class DrawingView extends View{

	 private float oldx = 0f;
	 private float oldy = 0f;
	 private Bitmap bmp = null;
	 private Canvas bmpCanvas;
	 private Paint paint;
	 
	
	 public DrawingView(Context context){
		 super(context);
		 //初期設定で作成してしまう。
		 paint = new Paint();
		 paint.setColor(Color.BLUE);
		 paint.setAntiAlias(true);
		 paint.setStyle(Paint.Style.STROKE);
		 paint.setStrokeWidth(6);
		 paint.setStrokeCap(Paint.Cap.ROUND);
		 paint.setStrokeJoin(Paint.Join.ROUND);
		}
		 
		 protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		 super.onSizeChanged(w,h,oldw,oldh);
		 bmp = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
		 bmpCanvas = new Canvas(bmp);
		 }
		 
		 private float getHOfEquilateralTriangle(float side){
			 float s = (float)(java.lang.Math.sqrt(3) / 2);
			 return side*s;
		 }
		 
		 private Path getTrianglePath(float x, float y, float width, float height){
			 Path path = new Path();
			 path.moveTo(x, y);
			 path.lineTo(x+width, y);
			 path.lineTo(x+(width/2), y-height);
			 path.close();
			 return path;
		 }
		 
		 public void onDraw(Canvas canvas) {
		 canvas.drawBitmap(bmp, 0, 0, null);
			/*
			Paint paint = new Paint();
			paint.setColor(0xff0000ff);
			paint.setStyle(Paint.Style.STROKE);
			paint.setColor(0xff0000ff);
			//paint.setStyle(Paint.Style.FILL);
			Path path = this.getTrianglePath(30, 100, 30, this.getHOfEquilateralTriangle(30));
			canvas.drawPath(path,paint);
			*/

		 }
		 
		 public boolean onTouchEvent(MotionEvent e){
			 
		 switch(e.getAction()){
		 case MotionEvent.ACTION_DOWN: //最初のポイント
		 oldx = e.getX();
		 oldy = e.getY();
		 break;
		 case MotionEvent.ACTION_MOVE: //途中のポイント
			 //bmpCanvas.drawCircle(oldx, oldy, 10, paint);
			 bmpCanvas.drawPath(this.getTrianglePath(oldx, oldy, 30, this.getHOfEquilateralTriangle(30)), paint);
			 
		 //bmpCanvas.drawLine(oldx, oldy, e.getX(), e.getY(), paint);
		 oldx = e.getX();
		 oldy = e.getY();
		 invalidate();
		 break;
		 default:
		 break;
		 }
		 
		 return true;
		 
		 }
		 
}