package com.example.assignment2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.view.KeyEvent;
import android.view.SurfaceHolder;


public class LanderThread extends Thread {

	// State flags
	private boolean isRunning = false;
	private boolean isCrashing = false;
	
	private SurfaceHolder surfaceHolder;
	private Context context;
	
	// Lander's Location
	private float currentX = 10;
	private float currentY = 10;
	
	// Falling Speed
	private final float MAX_FALLING_SPEED = 2f;
	private float speedX = 2f;
	private float speedY = MAX_FALLING_SPEED;
	private long pace = 100;
	private final float SAFETY_SPEED = 1.5f;
	
	// Fuel and Score
	private float currentFuel = 100f;
	
	// X and Y points
	int site1, site2;
	ArrayList<Float> xList;
	ArrayList<Float> yList;
	private final int pointsCnt = 7;
	private final int siteCnt = 2;
	ArrayList<Float> siteXList;
	ArrayList<Float> siteYList;
	
	enum LanderState {
		Neutral,
		Main,
		Left,
		Right	
	}
	
	enum Direction {
		Neutral,
		Up,
		Left,
		Right
	}
	private Direction direction = Direction.Neutral;
	
	// Sound Effect
	private MediaPlayer se;
	
	// Constructor
    public LanderThread(SurfaceHolder surfaceHolder, Context context){
    	this.surfaceHolder = surfaceHolder;
    	this.context = context;
    }
	@Override
	public synchronized void start() {
		synchronized (this) {
			isRunning = true;
			currentX = surfaceHolder.getSurfaceFrame().centerX() - 20;
		}
		super.start();
	}

	@Override
	public void run() {
		setXYPoints();
		setSitePoints();
		while (isRunning){
			Canvas canvas = this.surfaceHolder.lockCanvas();
			synchronized (surfaceHolder) {
				drawGround(canvas);
				drawFuelGauge(canvas);
				drawLander(canvas);
			}
			this.surfaceHolder.unlockCanvasAndPost(canvas);
			
			if (isCrashing) pace = 1000;
			try {
				sleep(pace);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		try {
			sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		((MainActivity)this.context).restart();
	}
    // Initialise X, Y points of the ground
    private void setXYPoints(){
    	// set X and Y points
    	Random rnd = new Random();
    	xList = new ArrayList<Float>();
    	yList = new ArrayList<Float>();
    	Rect frame = surfaceHolder.getSurfaceFrame();
    	int xMax = frame.width();
    	int yMax = (frame.height()/7)*6;
    	int yMin = (frame.height()/7)*3;
    	int x=0, y=0;
    	for (int i=0; i < pointsCnt; i++){
    		if (xList.size() == 0) xList.add(0f);
    		else if (i == (pointsCnt-1)){
    			xList.add((float)xMax);
    		}
    		else{
    			x = rnd.nextInt(xMax);
    			xList.add((float)x);
    		}
    		do{
    			y = rnd.nextInt(yMax);
    		}while (y < yMin);
    		yList.add((float)y);
    	}
    	Collections.sort(xList);
    }
    // Initialise positions of landing sites
	private void setSitePoints(){
		Random rnd = new Random();
		site1=0; site2=0;
		int max = pointsCnt;
		while (site1==0){
			site1 = rnd.nextInt(max-1)+1;
		}
		if (site1 > (max/2)){
			site2 = rnd.nextInt(max/2)+1;
		}else{
			site2 = site1 + (max/2);
		}
	}
	
	// Drawing the fuel gauge and score
	private void drawFuelGauge(Canvas canvas){
		Rect screen = surfaceHolder.getSurfaceFrame();
		Paint back = new Paint();
		back.setColor(Color.DKGRAY);
		Paint text = new Paint();
		text.setColor(Color.WHITE);
		canvas.drawText("Fuel:", 2, screen.height()-10, text);
		Paint gauge = new Paint();
		gauge.setColor(Color.BLUE);
		float gaugeWidth = (screen.width()-100)-30;
		float fuel = (currentFuel*gaugeWidth)/100 + 30;
		canvas.drawRect(30, screen.height()-20, fuel, screen.height()-10, gauge);
		// Draw Score
		canvas.drawText("Score:", screen.width()-90, screen.height()-10, text);
		canvas.drawText(Integer.toString((int)currentFuel), screen.width()-50, screen.height()-10, text);
	}
	
	// Drawing lander methods
	private void drawLander(Canvas canvas){
		
		if (direction == Direction.Left){
			if(currentFuel > 0) currentX -= speedX;
		}else if (direction == Direction.Right){
			if(currentFuel > 0) currentX += speedX;
		}
		if (direction == Direction.Up){
			if (speedY >= 0.3f){
				if(currentFuel > 0) speedY -= 0.1f;
			}
		}
		if (direction == Direction.Neutral){
			if (speedY < MAX_FALLING_SPEED){
				speedY += 0.3f;
			}
		}
		currentY += speedY;
		
		Bitmap lander = BitmapFactory.decodeResource(
				context.getResources(), R.drawable.craftmain);
		boolean collisionL = contains(
				(Float[])xList.toArray(new Float[]{}), (Float[])yList.toArray(new Float[]{}),
					currentX, currentY+lander.getHeight());
		boolean collisionR = contains(
				(Float[])xList.toArray(new Float[]{}), (Float[])yList.toArray(new Float[]{}),
					currentX+lander.getWidth(), currentY+lander.getHeight());
		if (collisionL || collisionR){
			if (isCrashing){
				// After the explosion
				Paint paint = new Paint();
				paint.setColor(Color.WHITE);
				canvas.drawText("FAILED!", currentX, currentY-20, paint);
				Bitmap wreckage = BitmapFactory.decodeResource(
						context.getResources(), R.drawable.wreckage);
				canvas.drawBitmap(wreckage, currentX, currentY, null);
				isRunning = false;				
			}else if (isLandingSuccess(lander.getWidth(), lander.getHeight())){
				// Lander succeeded to land. 
				Paint paint = new Paint();
				paint.setColor(Color.WHITE);
				canvas.drawText("SUCCESS!", currentX, currentY-20, paint);
				canvas.drawBitmap(lander, currentX, currentY, null);
				se = MediaPlayer.create(context, R.raw.landing);
				se.start();
				isRunning = false;
			}else{
				// Draw a crash image
				Bitmap explosion = BitmapFactory.decodeResource(
						context.getResources(), R.drawable.explosion);
				canvas.drawBitmap(explosion, currentX-20, currentY, null);
				se = MediaPlayer.create(context, R.raw.explosion);
				se.start();
				isCrashing = true;
			}
		}else{
			canvas.drawBitmap(lander, currentX, currentY, null);			
		}
		drawFire(lander, canvas, direction);
		return;
	}
	// Check the landing is success or not.
	private boolean isLandingSuccess(float width, float height){
		boolean isSuccess = false;
		for (int i=0; i < siteCnt*2; i+=2){
			Float[] xAry = new Float[]{siteXList.get(i), siteXList.get(i+1)};
			Float[] yAry = new Float[]{siteYList.get(i), siteYList.get(i+1)};
			boolean inSiteL = contains(xAry, yAry, currentX, currentY+height);
			boolean inSiteR = contains(xAry, yAry, currentX+width, currentY+height);
			isSuccess = (inSiteL && inSiteR);
			if (isSuccess) break;
		}
		return isSuccess && (speedY < SAFETY_SPEED);
	}
	// draw thrusters or main rocket
	private void drawFire(Bitmap lander, Canvas canvas, Direction direction){
		if (!isRunning || isCrashing) return;
		if (currentFuel == 0){
			direction = Direction.Neutral;
			return;
		}
		Bitmap fire = null;
		se = MediaPlayer.create(context, R.raw.fire);
		se.seekTo(2000);
		if (direction == Direction.Left){
			fire = BitmapFactory.decodeResource(
					context.getResources(), R.drawable.thruster);
			float left = currentX + (lander.getWidth() - 7);
			float top  = currentY + lander.getHeight();
			canvas.drawBitmap(fire, left, top, null);
			// reduce current fuel
			if (currentFuel >= 0) currentFuel--;
			se.start();
		}else if (direction == Direction.Right){
			fire = BitmapFactory.decodeResource(
					context.getResources(), R.drawable.thruster);
			float left = currentX;
			float top  = currentY + lander.getHeight();
			canvas.drawBitmap(fire, left, top, null);
			// reduce current fuel
			if (currentFuel >= 0) currentFuel--;
			se.start();
		}else if (direction == Direction.Up){
			fire = BitmapFactory.decodeResource(
					context.getResources(), R.drawable.main_flame);
			float left = currentX + ((lander.getWidth()/2)-6);
			float top  = currentY + lander.getHeight();
			canvas.drawBitmap(fire, left, top, null);
			// reduce current fuel
			if (currentFuel >= 0) currentFuel--;
			se.start();
		}
	}
	// Drawing Background including grounds methods
	private void drawGround(Canvas canvas){
		// Fill in the screen by the Mars bitmap
		BitmapDrawable drawable = (BitmapDrawable)context.getResources().getDrawable(R.drawable.mars);
		drawable.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
		drawable.setBounds(surfaceHolder.getSurfaceFrame());
		drawable.draw(canvas);
		
		// Draw space
		Paint paint = new Paint();
		paint.setColor(Color.BLACK);
		paint.setStyle(Style.FILL_AND_STROKE);
		canvas.drawPath(drawSpacePath(), paint);
		drawSiteNumbers(canvas);
	}
	// Drawing the boundary line.
	private Path drawSpacePath(){
    	// Draw boundary
		Path path = new Path();
		path.moveTo(0, 0);
		float x=0,y=0;
		int sitesToDraw = siteCnt;
		float width = 50;
		siteXList = new ArrayList<Float>();
		siteYList = new ArrayList<Float>();
		float curX = 0;
		float margin = 5; // a margin of error
		for (int i=0; i < pointsCnt; i++){
			float rest = surfaceHolder.getSurfaceFrame().width()-xList.get(i);
			if (((i==site1)||(i==site2)) || (rest < (width*sitesToDraw))){
				// Draw landing sites
				x = xList.get(i-1)+width;
				y = yList.get(i-1);	// set horizon
				xList.set(i, x);
				yList.set(i, y);
				siteXList.add(xList.get(i-1)-margin); // start
				siteXList.add(x+margin); // end
				siteYList.add(y);
				siteYList.add(y);	// adjust
				sitesToDraw--;
			}else{
				x = xList.get(i);
				y = yList.get(i);
				if (x < curX){
					x = curX;
					xList.set(i, curX);
				}
			}
			path.lineTo(x, y);
			curX = x;
		}
		path.lineTo(surfaceHolder.getSurfaceFrame().width(), 0);
		return path;
	}
	private void drawSiteNumbers(Canvas canvas){
		float width = 50;
		Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setColor(Color.WHITE);
		int number = 1;
		for (int i=0; i < siteCnt*2; i+=2){
			float x = siteXList.get(i)+(width/2);
			float y = siteYList.get(i)-20;
			canvas.drawText(Integer.toString(number++), x, y, paint);
		}
	}
	// Key up and down methods
	public void doKeyDown(int keyCode, KeyEvent event) {
		synchronized (surfaceHolder) {
			if (currentFuel == 0) {
				direction = Direction.Neutral;
				return;
			}
			switch (keyCode){
			case KeyEvent.KEYCODE_DPAD_LEFT:
				direction = Direction.Left;
				break;
			case KeyEvent.KEYCODE_DPAD_RIGHT:
				direction = Direction.Right;
				break;
			case KeyEvent.KEYCODE_DPAD_UP:
			case KeyEvent.KEYCODE_DPAD_CENTER:
				direction = Direction.Up;
				break;
			default:
				break;
			}
		}
	}
	public void doKeyUp(int keyCode, KeyEvent event) {
		synchronized (surfaceHolder) {
			switch (keyCode){
			case KeyEvent.KEYCODE_DPAD_LEFT:
			case KeyEvent.KEYCODE_DPAD_RIGHT:
			case KeyEvent.KEYCODE_DPAD_UP:
			case KeyEvent.KEYCODE_DPAD_CENTER:
				direction = Direction.Neutral;
				break;
			default:
				break;
			}
		}
	}
	// Collision Detection Method
    public boolean contains(Float[] xAry, Float[] yAry, float x0, float y0) {
        int crossings = 0;

        for (int i = 0; i < xAry.length - 1; i++)
        {
        	float x1 = xAry[i];
        	float x2 = xAry[i+1];

        	float y1 = yAry[i];
        	float y2 = yAry[i+1];

        	float dy = y2 - y1;
        	float dx = x2 - x1;

                float slope = 0;
                if (dx != 0) {
                        slope = (float)dy / dx;
                }

                boolean cond1 = (x1 <= x0) && (x0 < x2); // is it in the range?
                boolean cond2 = (x2 <= x0) && (x0 < x1); // is it in the reverse
                                                                                      
                boolean above = (y0 > slope * (x0 - x1) + y1); // point slope 

                if ((cond1 || cond2) && above) {
                        crossings++;
                }
        }
        return (crossings % 2 != 0); // even or odd
    }
}
