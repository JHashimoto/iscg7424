package com.example.assignment2;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.KeyEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

@SuppressLint("ViewConstructor")
public class LanderView extends SurfaceView implements SurfaceHolder.Callback{

	private LanderThread thread;
	public LanderView(Context context) {
		super(context);
		SurfaceHolder holder = getHolder();
		holder.addCallback(this);
		this.thread = new LanderThread(holder, context);
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {}
	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {}
	
	public void doStart(){
		thread.start();
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		thread.doKeyDown(keyCode, event);
		return true;
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		thread.doKeyUp(keyCode, event);
		return true;
	}
}
