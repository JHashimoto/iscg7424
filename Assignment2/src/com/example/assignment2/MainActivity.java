package com.example.assignment2;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ActionBar.LayoutParams;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.KeyEvent;
import android.view.Menu;
import android.widget.FrameLayout;

public class MainActivity extends Activity {
	private LanderView view;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		FrameLayout layout = (FrameLayout) findViewById(R.id.frameLayout);
		view = new LanderView(this);
		layout.addView(view, LayoutParams.MATCH_PARENT);
		getStartDialog().show();
	}

	private AlertDialog getStartDialog(){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Mars Lander");
		builder.setNegativeButton("START",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    	view.doStart();
                    }
                });
		builder.setPositiveButton("EXIT",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    	finish();
                    }
                });
		return builder.create();
	}
	public void restart(){
		Intent intent = getIntent();
		this.finish();
		startActivity(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		return view.onKeyDown(keyCode, event);
	}
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		return view.onKeyUp(keyCode, event);
	}
}
